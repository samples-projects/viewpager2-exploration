# android-components-architecture

Simple android project implementing new Google ViewPager.
In this sample we are displaying picture from UnSplash API

# Technology
This project is a picture gallery built with :
- [MVP pattern](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter)
- [RxJava](https://github.com/ReactiveX/RxJava)  
- [RxAndroid](https://github.com/ReactiveX/RxAndroid)  for leveraging specific features for Android  of Rx
- [Retrofit](https://square.github.io/retrofit/) for querying APIs easily
- [Glide](https://github.com/bumptech/glide) for image loading
- [ViewPager2](https://developer.android.com/jetpack/androidx/releases/viewpager2) 

Some other tools are used for project quality and commodity : 
- [Dagger2](https://github.com/google/dagger) for injecting dependencies
- [Ktlint](https://github.com/pinterest/ktlint) for code formatting following rules
- [Detekt](https://github.com/arturbosch/detekt) for static code analysis

# Architecture

```mermaid
graph LR
A((Activity)) --> B((Presenter))
B --> A
B --> C((Repository))
C --> D(API)
```

# Installation
- clone project repo
- register as developer to Unsplash
- Create an application for this project in your unsplash account
- copy that application access key
- open **gradle.properties** file create a variable named **picture_api_access_key** whose value will the access key surrounded with quotes

