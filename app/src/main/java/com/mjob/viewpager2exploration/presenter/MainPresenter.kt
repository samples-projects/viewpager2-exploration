package com.mjob.viewpager2exploration.presenter

import android.annotation.SuppressLint
import com.mjob.viewpager2exploration.SchedulerWrapper
import com.mjob.viewpager2exploration.data.repository.PictureRepository
import com.mjob.viewpager2exploration.ui.view.MainView
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val pictureRepository: PictureRepository,
    private val schedulerWrapper: SchedulerWrapper
) {
    lateinit var view: MainView

    fun attachView(mainView: MainView) {
        view = mainView
    }

    @SuppressLint("CheckResult")
    fun getPictures() {
        pictureRepository.getPictures()
            .subscribeOn(schedulerWrapper.ioScheduler())
            .observeOn(schedulerWrapper.uiScheduler())
            .subscribe(
                { pictures ->
                    view.showPictures(pictures)
                    view.onFinishLoading()
                }, { error ->
                    view.showErrorMessage(error.localizedMessage)
                })
    }
}
