package com.mjob.viewpager2exploration

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerWrapper {
    fun ioScheduler(): Scheduler {
        return Schedulers.io()
    }

    fun uiScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}