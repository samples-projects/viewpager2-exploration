package com.mjob.viewpager2exploration

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mjob.viewpager2exploration.ui.view.activity.MainActivity
import dagger.android.support.DaggerFragment
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun ImageView.loadImageFromUrl(
    url: String,
    onSuccessfulImageLoadingCallback: (() -> Unit)? = null,
    onFailedImageLoadingCallback: (() -> Unit)? = null
) {
    Glide.with(context)
        .load(url)
        .addListener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                onFailedImageLoadingCallback?.invoke()
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                onSuccessfulImageLoadingCallback?.invoke()
                return false
            }
        })
        .into(this)
}

fun DaggerFragment.parentActivity(): MainActivity {
    return (activity as MainActivity)
}

fun Date.dateToString(): String {
    val dateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH)
    print(dateFormat.format(this))
    return dateFormat.format(this)
}