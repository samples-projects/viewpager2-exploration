package com.mjob.viewpager2exploration.data.repository.impl

import com.mjob.viewpager2exploration.data.api.PictureApiService
import com.mjob.viewpager2exploration.data.model.Picture
import com.mjob.viewpager2exploration.data.repository.PictureRepository
import io.reactivex.Observable
import javax.inject.Inject

class RemotePictureRepository @Inject constructor(
    private val pictureApiService: PictureApiService,
    private val pictureApiAccessToken: String
) :
    PictureRepository {
    override fun getPictures(): Observable<List<Picture>> {
        return pictureApiService.getPictures("Client-ID $pictureApiAccessToken", PAGE, PAGE_SIZE)
    }

    companion object {
        const val PAGE = 1
        const val PAGE_SIZE = 15
    }
}