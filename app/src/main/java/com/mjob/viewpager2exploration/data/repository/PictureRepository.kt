package com.mjob.viewpager2exploration.data.repository

import com.mjob.viewpager2exploration.data.model.Picture
import io.reactivex.Observable

interface PictureRepository {
    fun getPictures(): Observable<List<Picture>>
}