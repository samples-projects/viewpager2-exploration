package com.mjob.viewpager2exploration.data.api

import com.mjob.viewpager2exploration.data.model.Picture
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PictureApiService {
    @GET("/photos")
    fun getPictures(
        @Header("Authorization") accessToken: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Observable<List<Picture>>
}