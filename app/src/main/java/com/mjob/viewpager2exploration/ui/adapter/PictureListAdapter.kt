package com.mjob.viewpager2exploration.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mjob.viewpager2exploration.R
import com.mjob.viewpager2exploration.data.model.Picture
import javax.inject.Inject

class PictureListAdapter @Inject constructor(private val pictures: List<Picture>) :
    RecyclerView.Adapter<PictureViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_picture, parent, false)
        return PictureViewHolder(view)
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        val picture = pictures[position]
        holder.bindTo(picture)
    }

    override fun getItemCount(): Int = pictures.size
}
