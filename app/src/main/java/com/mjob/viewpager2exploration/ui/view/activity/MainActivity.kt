package com.mjob.viewpager2exploration.ui.view.activity

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2.ORIENTATION_VERTICAL
import com.google.android.material.snackbar.Snackbar
import com.mjob.viewpager2exploration.R
import com.mjob.viewpager2exploration.data.model.Picture
import com.mjob.viewpager2exploration.presenter.MainPresenter
import com.mjob.viewpager2exploration.ui.adapter.PictureListAdapter
import com.mjob.viewpager2exploration.ui.view.MainView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import com.google.android.material.tabs.TabLayout
import com.mjob.viewpager2exploration.ui.adapter.TabLayoutMediator

class MainActivity : DaggerAppCompatActivity(), MainView {
    @Inject
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        presenter.attachView(this)
        presenter.getPictures()
        loadingProgressBar.visibility = View.VISIBLE

    }

    override fun showPictures(pictures: List<Picture>) {
        viewpager.orientation = ORIENTATION_VERTICAL
        viewpager.adapter = PictureListAdapter(pictures)
        val tabLayoutMediator =
            TabLayoutMediator(tabLayout, viewpager, true, object : TabLayoutMediator.OnConfigureTabCallback {
                override fun onConfigureTab(tab: TabLayout.Tab, position: Int) {
                }
            })
        tabLayoutMediator.attach()
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(layout, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onFinishLoading() {
        loadingProgressBar.visibility = View.GONE
    }
}
