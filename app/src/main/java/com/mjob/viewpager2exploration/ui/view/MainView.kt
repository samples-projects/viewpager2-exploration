package com.mjob.viewpager2exploration.ui.view

import com.mjob.viewpager2exploration.data.model.Picture

interface MainView {
    fun showPictures(pictures: List<Picture>)
    fun showErrorMessage(message: String)
    fun onFinishLoading()
}
