package com.mjob.viewpager2exploration.di

import com.mjob.viewpager2exploration.PictureApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = [AndroidSupportInjectionModule::class,
        ViewsModule::class,
        ApplicationModule::class]
)
interface ApplicationComponent : AndroidInjector<PictureApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<PictureApplication>()
}