package com.mjob.viewpager2exploration.di

import com.mjob.viewpager2exploration.ui.view.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
}