package com.mjob.viewpager2exploration.di

import com.mjob.viewpager2exploration.BuildConfig
import com.mjob.viewpager2exploration.SchedulerWrapper
import com.mjob.viewpager2exploration.data.api.PictureApiService
import com.mjob.viewpager2exploration.data.repository.PictureRepository
import com.mjob.viewpager2exploration.data.repository.impl.RemotePictureRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

@Module
class ApplicationModule {
    @Provides
    fun provideRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.PICTURE_API_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    fun providePictureApiService(retrofit: Retrofit): PictureApiService {
        return retrofit.create(PictureApiService::class.java)
    }

    @Provides
    fun provideNetworkExecutor(): Executor {
        return Executors.newFixedThreadPool(THREAD_NUMBERS)
    }

    @Provides
    fun provideRemotePictureRepository(
        pictureApiService: PictureApiService
    ): PictureRepository {
        return RemotePictureRepository(pictureApiService, BuildConfig.PICTURE_API_ACCESS_KEY)
    }

    @Provides
    fun provideSchedulerWrapper(): SchedulerWrapper {
        return SchedulerWrapper()
    }

    companion object {
        const val THREAD_NUMBERS = 5
    }
}